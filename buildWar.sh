#!/bin/sh
mvn clean compile -Dmaven.test.skip=true -Dpackaging.type=jar package
echo "-= extract =-"
unzip -q /Users/se/Desktop/MyHome/WORKSPACE/MPGProject/GWFEngine/target/GWFEngine-0.99.jar -d target/temp
echo "-= remove lib =-"
rm -rf target/temp/BOOT-INF/lib/*
echo "-= Repack =-"
cd target/temp
jar cfm ../GWFEngine.war META-INF/MANIFEST.MF BOOT-INF META-INF org
cd ../../
echo "-= =Complete -"
