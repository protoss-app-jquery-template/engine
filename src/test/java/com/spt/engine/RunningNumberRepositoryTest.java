package com.spt.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.spt.engine.entity.general.RunningType;
import com.spt.engine.repository.general.RunningNumberRepository;
import com.spt.engine.repository.general.RunningTypeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class RunningNumberRepositoryTest {

	static final Logger LOGGER = LoggerFactory.getLogger(RunningNumberRepositoryTest.class);
	
	@Autowired
	RunningNumberRepository runningNumberRepository;
	
	@Autowired
	RunningTypeRepository runningTypeRepository;
	
	
	@Test
	public void generateRunningNumber_NewRunningTypeNewPrefix() {
		String prefix = "TST";
		String runningTypeCode = "TST";
		
		RunningType runningType = runningTypeRepository.findByCode(runningTypeCode);
		assertNull(runningType);
		
		assertEquals(runningNumberRepository.count(),0l);
		
		String runningNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertNull(runningNumber);
	}
	
	@Test
	public void generateRunningNumber_ExistsRunningTypeNewPrefix() {
		String prefix = "EXP";
		String runningTypeCode = "EXP";
		
		RunningType runningType = runningTypeRepository.findByCode(runningTypeCode);
		assertNotNull(runningType);
		
		assertEquals(runningNumberRepository.count(),0l);
		
		String runningNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertEquals(runningNumber,"EXP1");
	}


	
	@Test
	public void generateRunningNumber_NewRunningTypeExistsPrefix() {
		String prefix = "TST";
		String runningTypeCode = "TST";
		
		RunningType runningType = runningTypeRepository.findByCode(runningTypeCode);
		assertNull(runningType);
		
		assertEquals(runningNumberRepository.count(),0l);
		
		String runningNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertNull(runningNumber);
	}
	

	
	@Test
	public void generateRunningNumber_ExistsRunningTypeExistsPrefix() {
		String prefix = "EXP";
		String runningTypeCode = "EXP";
		
		RunningType runningType = runningTypeRepository.findByCode(runningTypeCode);
		assertNotNull(runningType);
		
		assertEquals(runningNumberRepository.count(),0l);
		
		String runningNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertEquals(runningNumber,"EXP1");
		
		assertEquals(runningNumberRepository.count(),1l);
		
		String runningNumber2 = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertEquals(runningNumber2,"EXP2");
		

		assertEquals(runningNumberRepository.count(),1l);
		
	}
	

	
	@Test
	public void generateRunningNumber_ExistsRunningTypeExistsPrefix_Format000000() {
		String prefix = "APP";
		String runningTypeCode = "APP";
		
		RunningType runningType = runningTypeRepository.findByCode(runningTypeCode);
		assertNotNull(runningType);
		
		assertEquals(runningNumberRepository.count(),0l);
		
		String runningNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertEquals(runningNumber,"APP000001");
		
		assertEquals(runningNumberRepository.count(),1l);
		
		String runningNumber2 = runningNumberRepository.generateRunningNumber(runningType, prefix);
		assertEquals(runningNumber2,"APP000002");
		

		assertEquals(runningNumberRepository.count(),1l);
		
	}
}
