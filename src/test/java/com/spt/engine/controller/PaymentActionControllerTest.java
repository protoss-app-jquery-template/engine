package com.spt.engine.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.lang.reflect.Type;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class PaymentActionControllerTest {
	static final Logger LOGGER = LoggerFactory.getLogger(PaymentActionControllerTest.class);

	@Autowired
	protected WebApplicationContext wac;
	protected MockMvc mockMvc;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Before
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

    
    @Test
    public void financeActionPaid_Test() throws Exception {
    	try {
            mockMvc.perform(post("/paymentAction/")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"COMP_CODE\": \"1010\",\"AC_DOC_NO\": \"6500006124\",\"PSTNG_DATE\": \"20171010\",\"STATUS\": \"P\", \"DATE\": \"20171015\",\"TIME\": \"153021\",\"TAB_PAYMENT\": {\"item\": [{"+
									"\"VENDOR_NO\": \"M0006099\",\"HEADER_TXT\": \"EXP0001\",\"AC_DOC_NO\": \"6200004193\",\"AMT_DOCCUR\": \"1630.00\"},"+
									"{\"VENDOR_NO\": \"M0006099\",\"HEADER_TXT\": \"EXP0002\",\"AC_DOC_NO\": \"6200004092\",\"AMT_DOCCUR\": \"3800.00\"}]}}")
					)
            		.andExpect(status().isOk());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    

   
}
