package com.spt.engine.controller;

import com.spt.engine.service.QRCodeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StreamUtils;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QRCodeControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private QRCodeService qrCodeService;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testQRCodeServiceQrCodeGenerationSuccess () throws Exception {
        byte[] imageBlob = qrCodeService.generateQRCode("/This is a test", 256, 256);
        assertNotNull(imageBlob);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQRCodeServiceQrCodeGenerationErrorNullText () throws Exception {
        qrCodeService.generateQRCode(null, 256, 256);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQRCodeServiceQrCodeGenerationErrorEmptyText () throws Exception {
        qrCodeService.generateQRCode("", 256, 256);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQRCodeServiceQrCodeGenerationErrorInvalidWidth () throws Exception {
        qrCodeService.generateQRCode("/This is a test", 0, 256);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testQRCodeServiceQrCodeGenerationErrorInvalidHeight () throws Exception {
        qrCodeService.generateQRCode("/This is a test", 256, 0);
    }

    @Test
    public void testQRCodeControllerSuccess() throws Exception {

        byte[] testImage = StreamUtils.copyToByteArray(getClass().getResourceAsStream("/APP1010171102R000001.png"));

        mockMvc.perform(get("/qrcode?text=ewf.mitrphol.com:8001/GWF/qrcodescandata/karoons/APP1010171102R000001"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_PNG))
                .andExpect(header().string("Cache-Control", "max-age=1800"))
                .andExpect(content().bytes(testImage));
    }

    @Test
    public void testQRCodeControllerFail() throws Exception {

        byte[] testImage = StreamUtils.copyToByteArray(getClass().getResourceAsStream("/APP1010171102R000001.png"));

        mockMvc.perform(get("/qrcode?text="))
                .andExpect(status().is5xxServerError());
    }
}