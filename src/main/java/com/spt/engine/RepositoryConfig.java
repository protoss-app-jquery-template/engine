package com.spt.engine;

import com.spt.engine.entity.general.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Menu.class);
        config.exposeIdsFor(User.class);
        config.exposeIdsFor(Role.class);
        config.exposeIdsFor(Parameter.class);
        config.exposeIdsFor(ParameterDetail.class);
        config.exposeIdsFor(MasterData.class);
        config.exposeIdsFor(MasterDataDetail.class);
        config.exposeIdsFor(LocaleMessage.class);

    }
}
