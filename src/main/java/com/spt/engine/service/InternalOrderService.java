package com.spt.engine.service;

import java.util.List;
import java.util.Map;

public interface InternalOrderService {



    public List<Map<String,Object>> readDataInternalOrderFromSapInterface();
    public void moveFileWhenfetchAndSaveDataSuccess();
}
