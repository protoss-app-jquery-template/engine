package com.spt.engine.service.Impl;

import java.nio.charset.Charset;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Splitter;
import com.spt.engine.service.AbstractOrgSysEngineService;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.util.StringUtil;


@Service("EmplyeeProfileService")
public class EmployeeProfileServiceImpl extends AbstractOrgSysEngineService implements EmplyeeProfileService{

	static final Logger LOGGER = LoggerFactory.getLogger(EmployeeProfileServiceImpl.class);
	

	@Override
	public ResponseEntity<String> findEmployeeProfileByEmployeeCode(String employeeCode) {

		String url = "/profile/"+employeeCode;
		ResponseEntity<String> restResult = getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
        Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		
		String dataProfile = String.valueOf(userMap.get("profile")).replaceAll("Corp.,", "Corp").replaceAll("Co.,", "Co.")
				.replaceAll("Safety,H","Safety H").replaceAll("Safety , H","Safety H");
		dataProfile = StringUtil.trimStringByString(dataProfile,"{","}");

		Map<String, String> userProfileMap = Splitter.on(", ").withKeyValueSeparator("=").split(dataProfile);

		MediaType mediaType = new MediaType("application","json", Charset.forName("UTF-8"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);


		
		return new ResponseEntity(gson.toJson(userProfileMap), headers, HttpStatus.OK);
	}

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findEmployeeProfileByKeySearch(String keySearch) {

		String url = "/profile/search/"+keySearch;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findAccountByPaPsaAndKeySearch(String paPsa,String keySearch) {

		String url = "/profile/account/"+paPsa+"/"+keySearch;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findFinanceByPaPsaAndKeySearch(String paPsa,String keySearch) {

		String url = "/profile/finance/"+paPsa+"/"+keySearch;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

}
