package com.spt.engine.service;

import org.springframework.http.ResponseEntity;

public interface SendMailService {
	
	public ResponseEntity<String>   sendMail(String from,String fromAddress,String addressTo,String subject,String contentText,String ccAddress);
	public ResponseEntity<String>   sendMail(String from,String fromAddress,String addressTo,String subject,String contentText,String ccAddress,String[] pathfileLs, String[] fileNameLs);

}
