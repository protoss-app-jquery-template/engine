package com.spt.engine.database;


import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import com.spt.engine.entity.general.Menu;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.User;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.UserRepository;
import com.spt.engine.repository.general.RoleRepository;


//@Component
public class UserAuthorizeLoader implements CommandLineRunner {

	private final RoleRepository roleRepository;
	private final UserRepository repository;
	private final MenuRepository menuRepository;
	

	@Autowired
	public UserAuthorizeLoader(UserRepository repository,
			MenuRepository menuRepository,RoleRepository roleRepository	) {

		this.repository = repository;
		this.menuRepository = menuRepository;
		this.roleRepository = roleRepository;

	}

	@Override
	public void run(String... strings) throws Exception {
		
		
		/* Initial Role */
		Role roleUser = new Role();
		roleUser.setRoleName("USER");
		roleUser.setFlagActive(true);
		this.roleRepository.save(roleUser);
		
		Role roleAdmin = new Role();
		roleAdmin.setRoleName("ADMIN");
		roleAdmin.setFlagActive(true);
		this.roleRepository.save(roleAdmin);
		
		Role roleSuperAdmin = new Role();
		roleSuperAdmin.setRoleName("SUPER_ADMIN");
		roleSuperAdmin.setFlagActive(true);
		this.roleRepository.save(roleSuperAdmin);
		
		Role roleSsAdmin = new Role();
		roleSsAdmin.setRoleName("SS_ADMIN");
		roleSsAdmin.setFlagActive(true);
		this.roleRepository.save(roleSsAdmin);
		
		Role roleHR = new Role();
		roleHR.setRoleName("HR");
		roleHR.setFlagActive(true);
		this.roleRepository.save(roleHR);
		

		Role roleAccount = new Role();
		roleAccount.setRoleName("ACCOUNT");
		roleAccount.setFlagActive(true);
		this.roleRepository.save(roleAccount);
		

		Role roleOfficeAdmin = new Role();
		roleOfficeAdmin.setRoleName("OFFICE_ADMIN");
		roleOfficeAdmin.setFlagActive(true);
		this.roleRepository.save(roleOfficeAdmin);
		
		
		
		/* Setup User */
		
		
		User user = new User();
		user.setUsername("karoons");
		user.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user.setAccountNonLocked(true);
		user.setAccountNonExpired(true);
		user.setCredentialsNonExpired(true);
		user.setEmail("suriya_e@softsquaregroup.com");

		user.setRoles(new HashSet<Role>(){{
            add(roleUser);
        }});
		this.repository.save(user);

		User user4 = new User();
		user4.setUsername("boonchaip");
		user4.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user4.setAccountNonLocked(true);
		user4.setAccountNonExpired(true);
		user4.setCredentialsNonExpired(true);
		user4.setEmail("suriya_e@softsquaregroup.com");

		user4.setRoles(new HashSet<Role>(){{
			add(roleUser);
		}});
		this.repository.save(user4);

		User user5 = new User();
		user5.setUsername("praphaiporni");
		user5.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user5.setAccountNonLocked(true);
		user5.setAccountNonExpired(true);
		user5.setCredentialsNonExpired(true);
		user5.setEmail("suriya_e@softsquaregroup.com");

		user5.setRoles(new HashSet<Role>(){{
			add(roleAccount);
			add(roleUser);
		}});
		this.repository.save(user5);

		User user6 = new User();
		user6.setUsername("phanrapas");
		user6.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user6.setAccountNonLocked(true);
		user6.setAccountNonExpired(true);
		user6.setCredentialsNonExpired(true);
		user6.setEmail("suriya_e@softsquaregroup.com");

		user6.setRoles(new HashSet<Role>(){{
			add(roleUser);
		}});
		this.repository.save(user6);

		User user7 = new User();
		user7.setUsername("santis");
		user7.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user7.setAccountNonLocked(true);
		user7.setAccountNonExpired(true);
		user7.setCredentialsNonExpired(true);
		user7.setEmail("suriya_e@softsquaregroup.com");

		user7.setRoles(new HashSet<Role>(){{
			add(roleUser);
		}});
		this.repository.save(user7);

		User user8 = new User();
		user8.setUsername("krisdam");
		user8.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user8.setAccountNonLocked(true);
		user8.setAccountNonExpired(true);
		user8.setCredentialsNonExpired(true);
		user8.setEmail("suriya_e@softsquaregroup.com");

		user8.setRoles(new HashSet<Role>(){{
			add(roleUser);
		}});
		this.repository.save(user8);

		User user9 = new User();
		user9.setUsername("noppishnichs");
		user9.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user9.setAccountNonLocked(true);
		user9.setAccountNonExpired(true);
		user9.setCredentialsNonExpired(true);
		user9.setEmail("suriya_e@softsquaregroup.com");

		user9.setRoles(new HashSet<Role>(){{
			add(roleUser);
		}});
		this.repository.save(user9);
		
		
		User user1 = new User();
		user1.setUsername("ssadmin");
		user1.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user1.setFirstName("Kumphol");
		user1.setLastName("Papansunti");
		user1.setAccountNonLocked(true);
		user1.setAccountNonExpired(true);
		user1.setCredentialsNonExpired(true);
		user1.setEmail("suriya.ex@gmail.com");
		
		user1.setRoles(new HashSet<Role>(){{
            add(roleSsAdmin);
        }});
		this.repository.save(user1);
		
		
		User user2 = new User();
		user2.setUsername("admin");
		user2.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user2.setAccountNonLocked(true);
		user2.setAccountNonExpired(true);
		user2.setCredentialsNonExpired(true);
		user2.setEmail("suriya.ex@gmail.com");
		
		user2.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
        }});
		this.repository.save(user2);
		

		
		User user3 = new User();
		user3.setUsername("superadmin");
		user3.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		user3.setAccountNonLocked(true);
		user3.setAccountNonExpired(true);
		user3.setCredentialsNonExpired(true);
		user3.setEmail("suriya.ex@gmail.com");
		
		user3.setRoles(new HashSet<Role>(){{
            add(roleSuperAdmin);
        }});
		this.repository.save(user3);
		
		
		User userHR = new User();
		userHR.setUsername("sshr");
		userHR.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		userHR.setAccountNonLocked(true);
		userHR.setAccountNonExpired(true);
		userHR.setCredentialsNonExpired(true);
		userHR.setEmail("suriya.ex@gmail.com");
		
		userHR.setRoles(new HashSet<Role>(){{
            add(roleHR);
        }});
		this.repository.save(userHR);
		
		
		User userAccount = new User();
		userAccount.setUsername("ssaccount");
		userAccount.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		userAccount.setAccountNonLocked(true);
		userAccount.setAccountNonExpired(true);
		userAccount.setCredentialsNonExpired(true);
		userAccount.setEmail("suriya.ex@gmail.com");
		
		userAccount.setRoles(new HashSet<Role>(){{
            add(roleAccount);
        }});
		this.repository.save(userAccount);
		

		User userOfficeAdmin = new User();
		userOfficeAdmin.setUsername("ssofficeadmin");
		userOfficeAdmin.setAccessToken("21232f297a57a5a743894a0e4a801fc3");
		userOfficeAdmin.setAccountNonLocked(true);
		userOfficeAdmin.setAccountNonExpired(true);
		userOfficeAdmin.setCredentialsNonExpired(true);
		userOfficeAdmin.setEmail("suriya.ex@gmail.com");

		userOfficeAdmin.setRoles(new HashSet<Role>(){{
			add(roleOfficeAdmin);
		}});
		this.repository.save(userOfficeAdmin);

		
		//-----------------------Setup Menu ----------------------------
		Menu adminMenu = new Menu("RO01","icon icon-world", null, null, 1,true,null,  "Administrator", "ส่วนตั้งค่าผู้ดูแลระบบ");
		menuRepository.save(adminMenu);
		

		adminMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
        }});
		
		menuRepository.save(adminMenu);
		
		
		//-----------------------Sub Menu Administrator ----------------------------
				Menu userMaintenanceMenu = new Menu("AD01","icon icon-display", "/users/listView", "users", 1,false, adminMenu.getId(),"User Maintenance", "จัดการผู้ใช้");
				menuRepository.save(userMaintenanceMenu);
				

				userMaintenanceMenu.setRoles(new HashSet<Role>(){{
					add(roleAdmin);
		            add(roleSuperAdmin);
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(userMaintenanceMenu);
				//++++++++++++++++++++++++++++++
				
				Menu roleMaintenanceMenu = new Menu("AD02","icon icon-world", "/roles/listView", "roles", 2,false,adminMenu.getId(),  "Role Maintenance", "จัดการกลุ่มผู้ใช้");
				menuRepository.save(roleMaintenanceMenu);
				

				roleMaintenanceMenu.setRoles(new HashSet<Role>(){{
					add(roleAdmin);
		            add(roleSuperAdmin);
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(roleMaintenanceMenu);
				
				//++++++++++++++++++++++++++++++
				
				Menu menuMaintenanceMenu = new Menu("AD03","icon icon-world", "/menus/listViewByRole", "menus", 3,false,adminMenu.getId(),  "Menu Maintenance", "จัดการเมนู");
				menuRepository.save(menuMaintenanceMenu);
				

				menuMaintenanceMenu.setRoles(new HashSet<Role>(){{
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(menuMaintenanceMenu);
				
				//++++++++++++++++++++++++++++++
				
				Menu parameterMaintenanceMenu = new Menu("AD04","icon icon-world", "/parameters/listView", "parameters", 3,false,adminMenu.getId(),  "Parameter Maintenance", "จัดการพารามิเตอร์");
				menuRepository.save(parameterMaintenanceMenu);
				

				parameterMaintenanceMenu.setRoles(new HashSet<Role>(){{
		            add(roleSuperAdmin);
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(parameterMaintenanceMenu);

				
				//++++++++++++++++++++++++++++++
				
				Menu localeMessageMaintenanceMenu = new Menu("AD05","icon icon-world", "/localeMessage/listView?code=LB", "localeMessage", 5,false,adminMenu.getId(),  "Locale Management", "จัดการภาษา");
				menuRepository.save(localeMessageMaintenanceMenu);
				

				localeMessageMaintenanceMenu.setRoles(new HashSet<Role>(){{
		            add(roleSuperAdmin);
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(localeMessageMaintenanceMenu);
				
				   

				//++++++++++++++++++++++++++++++
				
				Menu monitorLogMenu = new Menu("AD06","icon icon-world", "/logMonitor", "logMonitor", 6,false,adminMenu.getId(),  "Monitor Log", "ดูล๊อก");
				menuRepository.save(monitorLogMenu);
				

				monitorLogMenu.setRoles(new HashSet<Role>(){{
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(monitorLogMenu);
				//++++++++++++++++++++++++++++++
				
				Menu boardcastMessageMenu = new Menu("AD07","icon icon-world", "/boardcast/boardcastMessage", "boardcast", 7,false, adminMenu.getId(),  "Board Cast Message", "ส่งข้อความแจ้ง");
				menuRepository.save(boardcastMessageMenu);
				

				boardcastMessageMenu.setRoles(new HashSet<Role>(){{
		            add(roleSuperAdmin);
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(boardcastMessageMenu);
				//++++++++++++++++++++++++++++++
				
				Menu parameterConfigureMenu = new Menu("AD08","icon icon-world", "/parameterConfig/listView", "parameterConfig", 8,false, adminMenu.getId(),  "Paramerter Configure", "กำหนดตั้งค่าให้พารามิเตอร์");
				menuRepository.save(parameterConfigureMenu);
				

				parameterConfigureMenu.setRoles(new HashSet<Role>(){{
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(parameterConfigureMenu);
				//++++++++++++++++++++++++++++++
				
				Menu masterDataConfigureMenu = new Menu("AD09","icon icon-world", "/masterDataConfig/listView", "masterDataConfig", 9,false,adminMenu.getId(),  "Master Data Configure", "กำหนดตั้งค่าให้ข้อมูลหลัก");
				menuRepository.save(masterDataConfigureMenu);
				

				masterDataConfigureMenu.setRoles(new HashSet<Role>(){{
		            add(roleSsAdmin);
		        }});
				
				menuRepository.save(masterDataConfigureMenu);

		Menu viewAllDocumentMenu = new Menu("AD10","icon icon-world", "/viewDoc/listView", "viewDoc", 10,false,adminMenu.getId(),  "View all document", "รายการเอกสารทั้งหมด");
		menuRepository.save(viewAllDocumentMenu);


		viewAllDocumentMenu.setRoles(new HashSet<Role>(){{
			add(roleSsAdmin);
		}});



		menuRepository.save(viewAllDocumentMenu);
				
		
		Menu masterdataMenu = new Menu("RO02",null, null, null, 2,true,null,  "Master Data", "แฟ้มข้อมูลหลัก");
		menuRepository.save(masterdataMenu);
		

		masterdataMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
        }});
		
		menuRepository.save(masterdataMenu);
		
		

		Menu locationDomesticMenu = new Menu("MS01","icon icon-display", "/locations/listView?code=D", "locations", 1,false,masterdataMenu.getId(),  "Location Domestic", "สถานที่ภายในประเทศ");
		menuRepository.save(locationDomesticMenu);


		locationDomesticMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
		}});

		menuRepository.save(locationDomesticMenu);

		Menu locationForeignMenu = new Menu("MS02","icon icon-display", "/locations/listView?code=F", "locations", 2,false,masterdataMenu.getId(),  "Location Foreign", "สถานที่ต่างประเทศ");
		menuRepository.save(locationForeignMenu);


		locationForeignMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
		}});

		menuRepository.save(locationForeignMenu);

		Menu routeDistanceMenu = new Menu("MS03","icon icon-display", "/routeDistances/listView", "routeDistances", 3,false,masterdataMenu.getId(),  "Route Distance", "ระยะทางระหว่างสถานที่");
		menuRepository.save(routeDistanceMenu);


		routeDistanceMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(routeDistanceMenu);

		Menu connectionGL = new Menu("MS04","icon icon-display", "/gl/listView", "gl", 4,false,masterdataMenu.getId(),  "Connection GL", "กำหนดเงื่อนไขGLสำหรับโครงการ");
		menuRepository.save(connectionGL);


		connectionGL.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(connectionGL);

		Menu conditionalIOMenu = new Menu("MS05","icon icon-display", "/conditionalIOs/listView", "conditionalIOs", 5,false,masterdataMenu.getId(),  "Conditional IO", "กำหนดเงื่อนไขการคุม IO");
		menuRepository.save(conditionalIOMenu);


		conditionalIOMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(conditionalIOMenu);


		Menu petrolAllowancePerMonthMenu = new Menu("MS06","icon icon-display", "/petrolAllowancePerMonths/listView", "petrolAllowancePerMonths", 6,false,masterdataMenu.getId(),  "Petrol Allowance Per Month", "กำหนดสิทธิ์น้ำมันเบนซิน ต่อเดือน");
		menuRepository.save(petrolAllowancePerMonthMenu);


		petrolAllowancePerMonthMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(petrolAllowancePerMonthMenu);





		Menu delegate = new Menu("MS08","icon icon-display", "/delegate/listView", "delegate", 8,false,masterdataMenu.getId(),  "Delegate", "การมอบอำนาจอนุมัติแทน");
		menuRepository.save(delegate);


		delegate.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(delegate);


		Menu dieselAllowancePerMonthMenu = new Menu("MS07","icon icon-display", "/dieselAllowancePerMonths/listView", "dieselAllowancePerMonths", 7,false,masterdataMenu.getId(),  "Diesel Allowance Per Month", "กำหนดสิทธิ์น้ำมันดีเซล ต่อเดือน");
		menuRepository.save(dieselAllowancePerMonthMenu);


		dieselAllowancePerMonthMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(dieselAllowancePerMonthMenu);


		Menu monthlyPhoneBillMenu = new Menu("MS07","icon icon-display", "/monthlyPhoneBills/listView", "monthlyPhoneBills", 7,false,masterdataMenu.getId(),  "Monthly Phone Bill", "กำหนดสิทธิ์ค่าโทรศัพท์ ต่อเดือน");
		menuRepository.save(monthlyPhoneBillMenu);


		monthlyPhoneBillMenu.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(monthlyPhoneBillMenu);

		Menu expenseType = new Menu("MS10","icon icon-display", "/expenseType/listView", "expenseType", 10,false,masterdataMenu.getId(),  "ExpenseType", "กำหนดประเภทค่าใช้จ่าย");
		menuRepository.save(expenseType);


		expenseType.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(expenseType);


		Menu employeeReplacement = new Menu("MS11","icon icon-display", "/employeeReplacement/listView", "employeeReplacement", 11,false,masterdataMenu.getId(),  "EmployeeReplacement", "กำหนดสิทธิ์ทำแทน");
		menuRepository.save(employeeReplacement);


		employeeReplacement.setRoles(new HashSet<Role>(){{
			add(roleAdmin);
			add(roleSuperAdmin);
			add(roleSsAdmin);
		}});

		menuRepository.save(employeeReplacement);





		Menu eApproveMenu = new Menu("RO03","icon icon-display", "/approve/approveMainMenu", "approve", 3,false,null,  "E-Approve", "ส่วนการขออนุมัติ");
		menuRepository.save(eApproveMenu);
		
		eApproveMenu.setRoles(new HashSet<Role>(){{
			add(roleUser);
			add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
        }});
		
		menuRepository.save(eApproveMenu);
		

		Menu eExpenseMenu = new Menu("RO04","icon icon-display", "/expense/expenseMainMenu", "expense", 4,false,null,  "E-Expense", "ส่วนการขอเคลียร์ค่าใช้จ่าย");
		menuRepository.save(eExpenseMenu);
		
		eExpenseMenu.setRoles(new HashSet<Role>(){{
			add(roleUser);
			add(roleAdmin);
            add(roleSuperAdmin);
            add(roleSsAdmin);
        }});
		
		menuRepository.save(eExpenseMenu);
		
		/*
		

		Menu reportMenu = new Menu("RO05",null, null, null, 5,true,null,  "Report", "รายงาน");
		menuRepository.save(reportMenu);
		
		reportMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
        }});
		
		menuRepository.save(reportMenu);
		
		Menu kpiApproveMenu = new Menu("RP01","icon icon-display", "/report/criteria/REP001", "report", 1,false,reportMenu.getId(),  "KPI Approve Report", "รายงานแสดงค่าชี้วัดของการขออนุญาต");
		menuRepository.save(kpiApproveMenu);
		
		kpiApproveMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
        }});
		
		menuRepository.save(kpiApproveMenu);
		
		Menu kpiExpenseMenu = new Menu("RP02","icon icon-display", "/report02/listView", "report02", 2,false,reportMenu.getId(),  "KPI Expense Report", "รายงานแสดงค่าชี้วัดของการขอเคลียร์ค่าใช้จ่าย");
		menuRepository.save(kpiExpenseMenu);
		
		kpiExpenseMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
        }});
		
		menuRepository.save(kpiExpenseMenu);
		
		Menu improvementApproveMenu = new Menu("RP03","icon icon-display", "/report03/listView", "report03", 3,false,reportMenu.getId(),  "Improvement Approve Report", "รายงานแสดงค่าชี้วัดของการขออนุญาต");
		menuRepository.save(improvementApproveMenu);
		
		improvementApproveMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
        }});
		
		menuRepository.save(improvementApproveMenu);
		
		Menu improvementExpenseMenu = new Menu("RP04","icon icon-display", "/report04/listView", "report04", 4,false,reportMenu.getId(),  "Improvement Expense Report", "รายงานแสดงค่าชี้วัดของการขอเคลียร์ค่าใช้จ่าย");
		menuRepository.save(improvementExpenseMenu);
		
		improvementExpenseMenu.setRoles(new HashSet<Role>(){{
            add(roleAdmin);
            add(roleSuperAdmin);
        }});
		
		menuRepository.save(improvementExpenseMenu);
		
		*/
		
		
	}

}