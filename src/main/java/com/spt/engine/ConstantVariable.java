package com.spt.engine;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 	Create by SiriradC. 2017.07.25
 */
public class ConstantVariable {
	
	/* constant about PER DIEM PROCESS */
	/* PER_DIEM_RATE */
	public static final Integer PER_DIEM_THIRTY_RATE		= 30;
	public static final Integer PER_DIEM_SEVENTY_RATE		= 70;
	public static final Integer PER_DIEM_MITPHOL_RATE		= 70;
	public static final Integer PER_DIEM_FULL_RATE			= 100;
	
	/* ROUDING */
	public static final Integer SCALE_2_DIGIT_ROUDING		= 2;
	public static final Integer SCALE_4_DIGIT_ROUDING		= 4;
	
	/* Constant */
	public static final Integer RATE_100_PERCENT			= 100;
	public static final String UNDER_SCORE					= "_";
	
	/* Time Constant */
	public static final Integer TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN			= 24;
	public static final String  TIME_HOURS_PER_DAY_FOR_DAY_FROM_HOUR		= "24:00:00";
	public static final Integer TIME_MINUTE_PER_HOURS 		= 60;
	public static final Integer TIME_SECOND_PER_HOURS 		= 60;
	public static final Integer TIME_MILLI_SECOND 			= 1000;
	
	
	public static final Integer NON_TAX_AMOUNT				= 240;
	
	
	
	/* Mileage Calculation */
	public static final Integer OTHER_RATE_DISTANCE_COUNT 	= 99999;
	public static final String  OTHER_DISTANCE              = "OtherRate";
	
	
	/* Master Data */
	public static final String  MASTER_DATA_APPROVE_TYPE_CODE			= "M002";
	public static final String  MASTER_DATA_PER_DIEM_DATA_CODE			= "M003";
	public static final String  MASTER_DATA_PER_DIEM_RISK_CODE			= "M004";
	public static final String  MASTER_DATA_REQUEST_STATUS				= "M005";
	public static final String  MASTER_DATA_ACTION_REASON				= "M006";
	public static final String  MASTER_DATA_PER_DIEM_FOREIGN_CODE		= "M007";
	public static final String  MASTER_DATA_DOCUMENT_TYPE_CODE			= "M008";
	public static final String  MASTER_DATA_DOCUMENT_STATUS		        = "M009";
	public static final String  MASTER_DATA_CAR_TYPE_CODE		        = "M010";
	public static final String  MASTER_DATA_HOTEL_CODE		        	= "M011";
	public static final String  MASTER_DATA_MILEAGE_CODE		        = "M013";
	public static final String  MASTER_DATA_AIRLINE_CODE		        = "M014";
	public static final String  MASTER_DATA_ATTACHMENT_TYPE		        = "M015";
	public static final String  MASTER_DATA_ZONE_CODE			        = "M016";
	public static final String  MASTER_DATA_CAR_LICENCE			        = "M017";
	public static final String  MASTER_DATA_SUBVENTION			        = "M018";
	public static final String  MASTER_DATA_ACCOUNT_TEAM			    = "M020";
	public static final String  MASTER_DATA_EMAIL_TEMPLATE			    = "M022";
	public static final String  MASTER_DATA_PERSONAL_ASSISTANCE			= "M023";
	

	/* Master Data Detail */
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_STATUS_CANCEL			= "CCL";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_STATUS_DRAFT			= "DRF";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE			= "CMP";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS		= "ONP";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_STATUS_REJECT			= "REJ";

	public static final String  MASTER_DATA_DETAIL_APPROVE_TYPE_DOMESTIC			= "0001";
	public static final String  MASTER_DATA_DETAIL_APPROVE_TYPE_FOREIGN				= "0002";
	public static final String  MASTER_DATA_DETAIL_APPROVE_TYPE_CAR_BOOKING			= "0003";
	public static final String  MASTER_DATA_DETAIL_APPROVE_TYPE_HOTEL_BOOKING		= "0004";
	public static final String  MASTER_DATA_DETAIL_APPROVE_TYPE_FLIGHT_BOOKING		= "0005";

	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_VERIFY		= "VRF";
	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_APPROVE		= "APR";
	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CANCEL		= "CXL";
	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CREATE		= "CRT";
	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT		= "REJ";
	public static final String  MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_COMPLETE		= "CMP";

	public static final String  MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE			= "APP";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE			= "ADV";
	public static final String  MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE			= "EXP";

	public static final String ACTION_STATE_VERIFY	= "VRF";
	public static final String ACTION_STATE_APPROVE	= "APR";
	public static final String ACTION_STATE_ACCOUNT	= "ACC";
	public static final String ACTION_STATE_FINANCE	= "FNC";
	public static final String ACTION_STATE_ADMIN	= "ADM";
	public static final String ACTION_STATE_HR		= "HR";
	public static final String ACTION_STATE_PAID	= "PAID";
	public static final String ACTION_STATE_NAME_PAID	= "Paid";



	public static final String JBPM_STATE_VERIFY 	= "Verifier";
	public static final String JBPM_STATE_APPROVE 	= "Approver";
	public static final String JBPM_STATE_ACCOUNT 	= "Account";
	public static final String JBPM_STATE_FINANCE 	= "Finance";
	public static final String JBPM_STATE_ADMIN		= "Admin";

	public static final String ROLE_OFFICE_VERIFY	= "Verify";
	public static final String ROLE_OFFICE_ADMIN	= "Office Admin";

	public static final String FLOW_STATUS_ACTIVE				    = "ACTIVE";
	public static final String FLOW_STATUS_INACTIVE				    = "NONE";
	public static final String FLOW_STATUS_COMPLETE				    = "COMPLETE";


	/* Parameter */
	public static final String  PARAMETER_RATE_INCOME			= "P201";
	public static final String  PARAMETER_LONG_TERM_AMOUNT		= "P200";
	public static final String  PARAMETER_PATH_FILE_ATTACHMENT	= "P103";
	public static final String  PARAMETER_HOTEL_RESERVATION 	= "P107";
	public static final String  PARAMETER_FLIGHT_RESERVATION	= "P108";
	public static final String  PARAMETER_CAR_RESERVATION	= "P109";
	public static final String  PARAMETER_USER_MANUAL	= "P110";

	/* Key of Return Map of Per Diem */
	public static final String	USD_KEY						= "usd";
	public static final String	BAHT_KEY					= "baht";
	public static final String	DAYS_KEY					= "days";
	public static final String	HOURS_KEY					= "hours";
	public static final String	MINUTE_KEY					= "minute";
	public static final String	START_DATE_KEY				= "startDate";
	public static final String	START_TIME_KEY				= "startTime";
	public static final String	END_DATE_KEY				= "endDate";
	public static final String	END_TIME_KEY				= "endTime";
	public static final String	RATE_TYPE_KEY				= "rateType";
	public static final String	SELLING_RATE_KEY			= "sellingRate";
	public static final String	NON_TAX_KEY					= "nonTax";
	public static final String	TAX_KEY						= "tax";
	public static final String	SUMMARY_KEY					= "summary";
	public static final String  LOCATION_KEY				= "location";
	public static final String  AMOUNT_KEY					= "amount";
	
	
	/* Calculate Function Parameter */
	public static final Integer	CALCULATE_DIFFERENCE_DAY	= (TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN * TIME_MINUTE_PER_HOURS * TIME_SECOND_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_DIFFERENCE_HOURS	= (TIME_MINUTE_PER_HOURS * TIME_SECOND_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_DIFFERENCE_MINUTE	= (TIME_MINUTE_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_CONVERT_HOURS_TO_MINUTE	= (TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN * TIME_MINUTE_PER_HOURS);
	
	
	/* For Attachment Encode File Name */
	public static final String DOCUMENT_STATUS_DRAFT 			= "D";
	public static final String DOCUMENT_STATUS_DRAFT_PREFIX		= "DRAFT";
	public static final String DOCUMENT_STATUS_REAL 			= "R";
	
	public static final Map<String,String> EXPORT_MASTER_DATA = new HashMap<String,String>(){{
		 put(MASTER_DATA_APPROVE_TYPE_CODE,"approveType.json");
		 put(MASTER_DATA_PER_DIEM_DATA_CODE,"perDiemRateDomistic.json");
		 put(MASTER_DATA_PER_DIEM_RISK_CODE,"perDiemRiskRate.json" );
		 put(MASTER_DATA_REQUEST_STATUS,"requestStatus.json" );
		 put(MASTER_DATA_ACTION_REASON,"actionReason.json" );
		 put(MASTER_DATA_PER_DIEM_FOREIGN_CODE,"perDiemForeignRate.json" );
		 put(MASTER_DATA_DOCUMENT_TYPE_CODE,"documentType.json" );
		 put(MASTER_DATA_DOCUMENT_STATUS,"documentStatus.json" );
		 put(MASTER_DATA_CAR_TYPE_CODE,"carType.json" );
		 put(MASTER_DATA_HOTEL_CODE,"hotel.json" );
		 put(MASTER_DATA_MILEAGE_CODE,"mileage.json" );
		 put(MASTER_DATA_AIRLINE_CODE,"airline.json" );
		 put(MASTER_DATA_ATTACHMENT_TYPE,"attachmentType.json" );
		 put(MASTER_DATA_ZONE_CODE,"zoneCode.json" );
		 put(MASTER_DATA_CAR_LICENCE,"carLicence.json" );
		 put(MASTER_DATA_SUBVENTION,"subvention.json" );

	}};

	public static final String PAID_STATUS = "PAID";
	public static final File PATCH_FILE_TEST_IO = new File("src/test/resources/fileCSVTest/internalOrderData_Test.csv");
	public static final File PATCH_FILE_TEST_TO_MOVE_BACKUP_DATA = new File("src/test/resources/fileCSVTest/backupCSVFile");
	public static final String PARAMETER_TO_SPLIT_CSV_FILE = ",";
	public static final String PARAMETER_TO_SPLIT_CSV_FILE_IO = "\\|";
	public static final File PATCH_FILE_TEST_REQUEST_FILE_TO_STAMP_STATUS = new File("src/test/resources/fileCSVTest/stampPaidStatusOfRequest.csv");

	public static final char EMPLOYEE_LEVEL_START = 'C';
	public static final String HEAD_OFFICE_CODE = "0001";
	public static final String APPROVE_TYPE_CODE_DOCUMENT_SET = "006";
	public static final String APPROVE_REFERENCE_CODE_DOMINESTIC = "0001";
	public static final String LOCATION_ZONE_TYPE_A = "A";
	public static final String LOCATION_ZONE_TYPE_B = "B";
	public static final Double MAX_COST_C1_TO_C5 = 600.0;
	public static final Double MAX_COST_C6_TO_C7 = 800.0;
	public static final Double MAX_COST_C8_TO_C9 = 1000.0;
	public static final Double MAX_COST_C10_UP = 999999999.0;
	public static final Double RATE_ZONE_TYPE_A = 300.0;
	public static final Double RATE_ZONE_TYPE_B = 200.0;

	public static final String RESERVATION_TYPE_HOTEL = "H";
	public static final String RESERVATION_TYPE_FLIGHT = "F";
	public static final String RESERVATION_TYPE_CAR = "C";
}
