package com.spt.engine.util;

import java.util.HashMap;
import java.util.Map;

public class TextUtil {

	public static Map<String,String> getAddressMap(String address) {
		// TODO Auto-generated method stub

		String postCode = "";
		String country = "-";
		Map<String,String> addressMap = new HashMap();
		try {
			address = address.trim();
			postCode = address.substring(address.length()-5);
			addressMap.put("postCode", postCode);
		} catch (Exception e) { 
			addressMap.put("postCode", "-");
		}
		

		try {
			address = address.trim();
			if(address.indexOf("อ.") >= 0){
				country = address.substring(address.indexOf("อ."), address.length()-5);
			}else if(address.indexOf("อำเภอ") >= 0){
				country = address.substring(address.indexOf("อำเภอ"), address.length()-5);
			}if(address.indexOf("แขวง") >= 0){
				country = address.substring(address.indexOf("แขวง"), address.length()-5);
			}
			
			addressMap.put("country", country);
		} catch (Exception e) {
			addressMap.put("country", "-");
		}
		

		try {
			address = address.trim();
			String addrss = address.replace(postCode, "").replace(country, "").trim();
			if(addrss.trim().length() == 0){
				addrss = "-";
			}
			addressMap.put("address", addrss);
		} catch (Exception e) { 
			addressMap.put("address", "-");
		}
		
		return addressMap;
	}

}
