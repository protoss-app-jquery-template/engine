package com.spt.engine.entity.general;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class LocaleMessage {
	
	private @Id @GeneratedValue(strategy=GenerationType.TABLE) Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	
	private String groupCode;
	private String code;
	private @Formula("CONCAT(variable_name1,' ',variable_name2,' ',variable_name3,' ',variable_name4,' ',variable_name5,' ',variable_name6,' ',variable_name7,' ',variable_name8)") String fullText;
	
	private String variableName1;
	private String variableName2;
	private String variableName3;
	private String variableName4;
	private String variableName5;
	private String variableName6;
	private String variableName7;
	private String variableName8;
	
	public LocaleMessage(String groupCode, String code, String variableName1, String variableName2) {
		super();
		this.groupCode = groupCode;
		this.code = code;
		this.variableName1 = variableName1;
		this.variableName2 = variableName2;
	}
	
	
	
}
