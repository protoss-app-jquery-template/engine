package com.spt.engine.repository.general;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.RunningType;

public interface RunningTypeRepository  extends JpaSpecificationExecutor<RunningType>, JpaRepository<RunningType, Long>, PagingAndSortingRepository<RunningType, Long> {

	RunningType findByCode(@Param("code") String code);
}
