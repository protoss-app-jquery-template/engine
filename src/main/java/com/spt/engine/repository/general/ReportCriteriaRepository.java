package com.spt.engine.repository.general;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spt.engine.entity.general.ReportCriteria;


public interface ReportCriteriaRepository extends JpaSpecificationExecutor<ReportCriteria>, JpaRepository<ReportCriteria, Long>, PagingAndSortingRepository<ReportCriteria, Long>  {


	
}
