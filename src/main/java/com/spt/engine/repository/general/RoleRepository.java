package com.spt.engine.repository.general;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.Role;

import java.util.List;


public interface RoleRepository extends JpaSpecificationExecutor<Role>, JpaRepository<Role, Long>, PagingAndSortingRepository<Role, Long>  {

	Role findByRoleName(@Param("roleName") String roleName);
	
	Page<Role> findByRoleNameIgnoreCaseContaining(
			@Param("roleName") String roleName,
			Pageable pageable);




	@Query("select DISTINCT u from Role u ")
	List<Role> findAllRole();


}
