package com.spt.engine.repository.general;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.ParameterDetail;


public interface ParameterDetailRepository extends JpaSpecificationExecutor<ParameterDetail>, JpaRepository<ParameterDetail, Long>, PagingAndSortingRepository<ParameterDetail, Long>  {

	@Query("select DISTINCT u from ParameterDetail u left join u.parameter r where  lower(u.code) like CONCAT('%',lower(:code),'%') and "
			+ " lower(u.name) like CONCAT('%',lower(:name),'%')  and "
			+ " r.id in :parameter ")
	Page<ParameterDetail> findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndParameterIn(
			@Param("code") String code,
			@Param("name") String name,
			@Param("parameter") List<Long> parameter,
			Pageable pageable);
	
	@Query("select DISTINCT u from ParameterDetail u left join u.parameter r where r.code in :parameter order by u.id ")
	List<ParameterDetail> findByParameterCodeIn( @Param("parameter") List<String> parameter);

	@Query("select DISTINCT u from ParameterDetail u where lower(u.code) like concat('%',lower(:code))")
	List<ParameterDetail> findByCodeIgnoreCaseContaining(
		@Param("code") String code
	);
}
