package com.spt.engine.repository.general;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportCriteria;


public interface ReportConfigurationRepository extends JpaSpecificationExecutor<ReportConfiguration>, JpaRepository<ReportConfiguration, Long>, PagingAndSortingRepository<ReportConfiguration, Long>  {

	ReportConfiguration findByCode(@Param("code") String code);
	

	@Query("select DISTINCT u from ReportCriteria u left join u.reportConfiguration r where  r.code in :reportConfiguration ")
	Page<ReportCriteria> findReportConfigurationIn(
			@Param("reportConfiguration") List<String> reportConfiguration,
			Pageable pageable);
}
